<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Products</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
  integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>

<body>
  <nav class="navbar navbar-expand-lg bg-dark py-4 fw-bold">
    <div class="container">
      <a class="navbar-brand text-white" href="#">Rashel's Products</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
      aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link text-white" href="#">ABOUT</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <section id="title">
    <h1 class="text-center fw-bold mt-3">Product List</h1>
  </section>

  <main>
    <section id="products">
      <div class="container mb-3">
        <div class="row row-cols-1 row-cols-md-3 g-4">
          <?php
          // api
          $url = "https://dummyjson.com/products?limit=24&select=images,title,price,description,stock,brand";

          $curl = curl_init($url);

          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

          $response = curl_exec($curl);

          curl_close($curl);

          // cek data api
          // echo "<pre>";
          // print_r($response);
          // echo "</pre>";

          // ke json
          $data = json_decode($response, true);

          if ($data !== null && isset($data['products'])) {
            foreach ($data['products'] as $product) {
              // echo products
              echo "<div class='col'>";
              echo "<div class='card h-100'>";
              // cek ada atau nggak + echo 
              if (isset($product['images'])) {
                echo "<img src='" . $product['images'][0] . "' class='card-img-top'
                alt='Product Image' style='width: 100%; height: 300px; object-fit: fill;'>";
              }
              echo "<div class='card-body'>";

              if (isset($product['title'])) {
                echo "<h5 class='card-title'>" . $product['title'] . "</h5>";
              }

              if (isset($product['description'])) {
                echo "<p class='card-text'>" . $product['description'] . "</p>";
              }

              if (isset($product['price'])) {
                echo "<p class='card-text'>Price: $" . $product['price'] . "</p>";
              }

              if (isset($product['stock'])) {
                echo "<p class='card-text'>Stock: " . $product['stock'] . "</p>";
              }

              if (isset($product['brand'])) {
                echo "<p class='card-text'>Brand: " . $product['brand'] . "</p>";
              }
              echo "</div>"; //  card-body
              echo "</div>"; //  card
              echo "</div>"; //  col
            }
          } else {
            echo '<div class="alert-danger" role="alert">Gagal mengambil data</div>';
          }
          ?>
        </div> <!-- row -->
      </div> <!-- container -->
    </section>
  </main>

  <!-- js bootstrap -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
  integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>

</html>